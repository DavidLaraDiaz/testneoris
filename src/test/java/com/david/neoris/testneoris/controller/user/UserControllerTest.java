package com.david.neoris.testneoris.controller.user;

import com.david.neoris.testneoris.enums.user.UserIdentificationEnum;
import com.david.neoris.testneoris.payload.user.UserForm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @Order(1)
    void getAllUsers() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/user/all");
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }

    @Test
    @Order(2)
    void getUsersActivesPaged() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/user");
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }

    @Test
    @Order(6)
    void getByIdUser() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/user/4");
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }

    @Test
    @Order(3)
    void createUser() throws Exception {
        UserForm userForm = new UserForm();
        userForm.setName("user name");
        userForm.setLastname("user last name");
        userForm.setEmail("user@yopmail.com");
        userForm.setPhone("1234567");
        userForm.setRol("TEST");

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(userForm);
        RequestBuilder request = MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }

    @Test
    @Order(4)
    void updateUser() throws Exception {
        UserForm userForm = new UserForm();
        userForm.setName("user update");
        userForm.setLastname("user last name");
        userForm.setEmail("user@yopmail.com");
        userForm.setPhone("1234567");
        userForm.setRol("TEST");
        userForm.setIdentificationType(UserIdentificationEnum.PASAPORTE);
        userForm.setIdentification("43211234");

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(userForm);
        RequestBuilder request = MockMvcRequestBuilders.put("/user/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }

    @Test
    @Order(5)
    void toggleStatusUser() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.put("/user/status/4");
        MvcResult result = mvc.perform(request).andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String body = result.getResponse().getContentAsString();
        System.out.println("---------RESPONSE--------- ");
        System.out.println(body);
        assertNotNull(body);
    }
}