package com.david.neoris.testneoris.payload.user;

import com.david.neoris.testneoris.enums.user.UserIdentificationEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserForm {

    @NotNull(message = "El nombre es obligatorio")
    private String name;

    @NotNull(message = "El apellido es obligatorio")
    private String lastname;

    @NotNull(message = "El correo electronico es obligatorio")
    private String email;

    private String phone;

    @NotNull(message = "El ROL es obligatorio")
    private String rol;

    private UserIdentificationEnum identificationType;

    private String identification;

    public UserForm() {
    }

}
