package com.david.neoris.testneoris.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * this class is used to create the basic audit object for all tables in the database
 * @author David Lara
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BasicEntity {

    @CreatedDate
    @Column( name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @CreatedBy
    @JsonIgnore
    @Column( name = "created_by", nullable = false, updatable = false)
    private Integer createdBy;

    @JsonIgnore
    @Column( name = "updated_at")
    private Date updatedAt;

    @JsonIgnore
    @Column( name = "updated_by")
    private Integer updatedBy;

    @JsonIgnore
    @Column( name = "delete_at")
    private Date deleteAt;

    @JsonIgnore
    @Column( name = "delete_by")
    private Integer deleteBy;

    @NotNull
    @Column( name = "status")
    private Integer status;

    public BasicEntity() {
        this.createdAt = new Date();
        this.status = 1;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getDeleteAt() {
        return deleteAt;
    }

    public void setDeleteAt(Date deleteAt) {
        this.deleteAt = deleteAt;
    }

    public Integer getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Integer deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
