package com.david.neoris.testneoris.controller.user;

import com.david.neoris.testneoris.entity.user.UserEntity;
import com.david.neoris.testneoris.exception.NeorisRuntimeException;
import com.david.neoris.testneoris.payload.user.UserForm;
import com.david.neoris.testneoris.service.user.UserService;
import com.david.neoris.testneoris.utils.GeneralBodyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Optional;

/**
 * @author David Lara
 * @date 08/06/2022
 *
 * This class content the endpoints for ADMIN CRUD UserEntity
 *
 */


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;

    private final String OBJECT_NAME = "USUARIO";


    @GetMapping("/all")
    public ResponseEntity<GeneralBodyResponse<Object>> getAllUsers(

    ){
        try {
            List<UserEntity> users = this.userService.getAll();

            if (users.isEmpty()){
                return ResponseEntity.noContent().build();
            }

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            users,
                            messageSource.getMessage(
                                    "general.listed",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }

    @GetMapping
    public ResponseEntity<GeneralBodyResponse<Object>> getUsersActivesPaged(
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ){
        try {
            Page<UserEntity> users = this.userService.getAllPaged(pageable);

            if (users.isEmpty()){
                return ResponseEntity.noContent().build();
            }

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            users,
                            messageSource.getMessage(
                                    "general.listed",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }


    @GetMapping("/{userId}")
    public ResponseEntity<GeneralBodyResponse<Object>> getByIdUser(
            @PathVariable("userId") Integer userId
    ){
        try {
            Optional<UserEntity> user = this.userService.findById(userId);

            if (!user.isPresent()){
                return ResponseEntity.noContent().build();
            }

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            user,
                            messageSource.getMessage(
                                    "general.query",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }


    @PostMapping()
    public ResponseEntity<GeneralBodyResponse<Object>> createUser(
            @Valid @RequestBody @NotEmpty UserForm form,
            BindingResult bindingResult
    ){

        if (bindingResult.hasErrors()){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            null,
                            messageSource.getMessage(
                                    "general.error.form",
                                    new String[]{OBJECT_NAME},
                                    null
                            ),
                            bindingResult.getAllErrors()
                    ), HttpStatus.BAD_REQUEST
            );
        }

        try {
            UserEntity user = this.userService.save(form);

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            user,
                            messageSource.getMessage(
                                    "general.created",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }


    @PutMapping("/{userId}")
    public ResponseEntity<GeneralBodyResponse<Object>> updateUser(
            @PathVariable("userId") Integer userId,
            @Valid @RequestBody @NotEmpty UserForm form,
            BindingResult bindingResult
    ){

        if (bindingResult.hasErrors()){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            null,
                            messageSource.getMessage(
                                    "general.error.form",
                                    new String[]{OBJECT_NAME},
                                    null
                            ),
                            bindingResult.getAllErrors()
                    ), HttpStatus.BAD_REQUEST
            );
        }

        try {
            UserEntity user = this.userService.save(userId, form);

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            user,
                            messageSource.getMessage(
                                    "general.updated",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }


    @PutMapping("/status/{userId}")
    public ResponseEntity<GeneralBodyResponse<Object>> toggleStatusUser(
            @PathVariable("userId") Integer userId
    ){


        try {
            UserEntity user = this.userService.toggleStatusById(userId);

            return ResponseEntity.ok(
                    new GeneralBodyResponse<>(
                            user,
                            messageSource.getMessage(
                                    "general.updated.status",
                                    new String[]{OBJECT_NAME},
                                    null
                            )
                    )
            );

        }catch (NeorisRuntimeException e){
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getDetail().getCode(),
                            e.getMessage()
                    ),HttpStatus.BAD_REQUEST
            );

        }

        catch (Exception e) {
            return new ResponseEntity<>(
                    new GeneralBodyResponse<>(
                            e.getCause(),
                            e.getMessage()
                    ), HttpStatus.BAD_REQUEST
            );
        }

    }


}
