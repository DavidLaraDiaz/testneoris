package com.david.neoris.testneoris.service.user;


import com.david.neoris.testneoris.entity.user.UserEntity;
import com.david.neoris.testneoris.payload.user.UserForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UserEntity save(UserForm form);
    UserEntity save(UserEntity user);
    UserEntity save(Integer userId, UserForm form);

    Optional<UserEntity> findById(Integer userId);
    Optional<UserEntity> findByEmailAndStatus(String email, Integer status);

    UserEntity toggleStatusById(Integer userId);

    List<UserEntity> getAll();
    Page<UserEntity> getAllPaged(Pageable pageable);


}
